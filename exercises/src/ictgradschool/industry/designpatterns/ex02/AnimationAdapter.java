package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Iterator;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public class AnimationAdapter implements TreeModel { // law: implements first

    private NestingShape Shape ;

    public AnimationAdapter(NestingShape root) {
        Shape = root;
    }



    @Override
    public Object getRoot() {
        return Shape;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if(parent instanceof NestingShape){
            NestingShape dir = (NestingShape)parent;
            result= dir.shapeAt(index);
        }

        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape shapes = (Shape) parent;

        if (shapes instanceof Shape){
            NestingShape nestingShape = (NestingShape) shapes;
            result = nestingShape.shapeCount();
        }

        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        // not need
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape dir = (NestingShape) parent;
            return dir.indexOf((Shape)child);
        }
        return indexOfChild;


    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
       // not need.
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        //not need.
    }
}

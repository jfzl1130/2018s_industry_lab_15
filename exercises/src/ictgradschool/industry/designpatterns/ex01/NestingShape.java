package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;
import java.util.List;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class NestingShape extends Shape {


    List<Shape>shapes= new ArrayList<Shape>();


    public NestingShape(){
        super();
    }


    public NestingShape(int x, int y){
        super(x,y);
    }


    public NestingShape(int x, int y, int deltaX, int deltaY){
        super(x,y,deltaX,deltaY);


    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int
            height){

        super(x,y,deltaX,deltaY,width,height);

    }

    public void move(int width, int height){

        super.move(width,height);
        for (Shape childShape:shapes){
            childShape.move(fWidth,fHeight);
        }
    }

    public void paint(Painter painter){

        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX,fY);   //law: shift the painter.
        for (Shape childShape:shapes){ // law: loop gothrough all the child nesting shape.
            childShape.paint(painter);
        }
        painter.translate(-fX,-fY);  //law: shift back the painter.

    }

    public void add(Shape child) throws IllegalArgumentException{   // law: check if it out of boundary



        if(child.parent()!= null){throw new IllegalArgumentException("The Shape argument is already a child within a NestingShape.");
        }
        else if(child.fX<0||child.fY<0){throw new IllegalArgumentException( "The Shape that will not fit within the bounds of the proposed NestingShape object.");
        }
        else if(child.fX+child.fWidth>fWidth||child.fY+child.fHeight>fHeight){
            throw new IllegalArgumentException( "The Shape that will not fit within the bounds of the proposed NestingShape object.");
        }
        else{
        shapes.add(child);
        child.setParent( this);}

    }

    public void remove(Shape child){



        if( shapes.remove( child ) )

        child.setParent( null );


    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException{

        if(index>=shapes.size()|| index<0) {
            throw new IndexOutOfBoundsException();
        }
        return shapes.get(index);
    }

    public int shapeCount(){
        return shapes.size();
    }

    public int indexOf(Shape child){

        return shapes.indexOf(child);
    }
    public boolean contains(Shape child){
        return shapes.contains(child);
    }

}
